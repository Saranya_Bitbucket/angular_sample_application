var app = angular.module('mainApp',['ngRoute']);

app.config(['$routeProvider',function($routeProvider){
	$routeProvider
	.when('/',{
		template: 'Welcome to Home Page !'
	})
	.when('/page1',{
		template: 'Welccome to Page 1 !'
	})
	.otherwise({
		redirectTo: '/'
	});
}]);